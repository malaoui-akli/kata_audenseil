package com.am;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CompteMain {
	
	public final static  String VERSEMENT = "VERSEMENT";
	public final static  String RETRAIT = "RETRAIT";
	

public static void main(String... arg) {
	LocalDateTime today = LocalDateTime.now();
		//Cr�ation d'un client
		Client client = new Client(11, "Ali", "Baba");
		
		//Cr�ation d'un compte
		Compte compte = new Compte(11, "456789", client);
		//Ajouter un compte � un client
		client.setCompte(compte);
		
		
		//Affichage de propri�taire de compte 
		System.out.println("Client : " + client.getNom()+ " " + client.getPrenom() );
		
		//Affichage l'identifient de compte
		System.out.println("Compte : " + compte.getNumero() + "\n");
		
		
		
		//Solde initial
		System.out.println("Solde initial  --------------------------------------------------  " + compte.getSolde()+ " � \n");
				
				
		//Effectuer une op�ration de versement
		Operation op1 = new Operation(today, VERSEMENT, 1000, compte);
		compte.addOperation(op1);
		
		//Effectuer une op�ration de versement
		Operation op2 = new Operation(today, VERSEMENT, 5000, compte);
		compte.addOperation(op2);
		
		//Effectuer une op�ration de retrait
		Operation op3 = new Operation(today, RETRAIT, 1000, compte);
		compte.addOperation(op3);
		
		//Effectuer une op�ration de retrait
		Operation op4 = new Operation(today, RETRAIT, 2000, compte);
		compte.addOperation(op4);
		
		
	
		
		
		//Affichage l'historique de compte
		System.out.println("Op�rations ");
		
		System.out.println("Type d'op�ration \t  Date \t\t\t\t  Montant \t ");
		System.out.println("________________________________________________________________________");
		for(Operation op : compte.getOperations()) {
			System.out.println(op.getType() + "\t\t " + op.getDate().format(DateTimeFormatter.ofPattern("d MMM uuuu HH:mm:ss")) + "\t\t" + op.getMontant()+" �");
		}
		
		System.out.println("________________________________________________________________________");
		
		System.out.println("Solde \t\t\t\t\t\t\t\t" + compte.getSolde()+" �");
	}

	
	
}
